import React from "react";
import ProductImage from "./productImage";
import ProductName from "./productName";
import ProductDescription from "./productDescription";
import SelectAddOn from "./selectAddOn";
import SpecialNotes from "./specialNotes";

function ProductDetail() {
  return (
    <div className="product_detail">
      <ProductImage />
      <ProductName />
      <ProductDescription />
      <div className="extra_selection">
        <div id="accordion2" className="accordion  mt-4 mb-4">
          <div className="card  mb-0">

            {
              [1, 2,].map(productdetail => {
                return (
                  <SelectAddOn  key={productdetail}/>
                )
              })
            }

          </div>
        </div>
        <SpecialNotes />
      </div>
    </div>
  );
}

export default ProductDetail;
