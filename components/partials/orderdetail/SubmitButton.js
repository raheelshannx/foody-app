import useTranslation from "../../../services/useTranslation";
import Link from "next/link";
function SubmitButton() {
    const { re_order } = useTranslation();
    return (
        <Link Link href={{ pathname: '/'}}>
        <button className="primary  mb-3">{re_order}</button>
        </Link>
    );
}
export default SubmitButton;