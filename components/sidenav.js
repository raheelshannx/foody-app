import React from "react";
import Link from "next/link";
import useTranslation from "../services/useTranslation";
import { useSelector } from "react-redux";
import { useEffect } from "react";

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
function logOut() {
  localStorage.removeItem("token");
  document.getElementById("mySidenav").style.width = "0";
}
function MySideNav() {
  const {
    order_status,
    Branches,
    contact_us,
    my_account,
    Menu,
    logout,
    login,
  } = useTranslation();
  const currencies = useSelector((state) => state.currencies);

  const renderCurrencies = () => {
    if (currencies.length > 1) {
      return (
        <select className="custom-select">
          {currencies.map((currency) => {
            return <option key={currency.name}>{currency.name}</option>;
          })}
        </select>
      );
    } else {
      return "";
    }
  };

  const renderAuth = () => {
    if (process.browser) {
      if (
        localStorage != undefined &&
        localStorage.getItem("token") != undefined
      ) {
        return (
          <li>
            <Link href="/myaccount">
              <a href="#" onClick={closeNav}>
                <span
                  className="iconify"
                  data-icon="ant-design:user-outlined"
                ></span>
                {my_account}
              </a>
            </Link>
            <Link href="/">
              <a href="#" onClick={logOut}>
                <span
                  className="iconify"
                  data-icon="ant-design:user-outlined"
                ></span>
                {logout}
              </a>
            </Link>
          </li>
        );
      } else {
        return (
          <li>
            <Link href="/loginregister">
              <a href="#" onClick={closeNav}>
                <span
                  className="iconify"
                  data-icon="ant-design:user-add-outlined"
                ></span>
                {login}
              </a>
            </Link>
          </li>
        );
      }
    } else {
      return (
        <li>
          <Link href="/loginregister">
            <a href="#" onClick={closeNav}>
              <span
                className="iconify"
                data-icon="ant-design:user-add-outlined"
              ></span>
              {login}
            </a>
          </Link>
        </li>
      );
    }
  };

  return (
    <div>
      <div id="mySidenav" className="sidenav">
        <a href="#" className="closebtn" onClick={closeNav}>
          &times;
        </a>
        <ul className="side_menu">
          <li>
            <Link href="/">
              <a href="#" onClick={closeNav}>
                <span className="iconify" data-icon="bx:bx-home-circle"></span>
                {Menu}
              </a>
            </Link>
          </li>
          {renderAuth()}
          <li>
            <Link href="/branches">
              <a href="#" onClick={closeNav}>
                <span className="iconify" data-icon="bx:bx-git-branch"></span>
                {Branches}
              </a>
            </Link>
          </li>
          <li>
            <Link href="/orderstatus">
              <a href="#" onClick={closeNav}>
                <span className="iconify" data-icon="mdi:go-kart-track"></span>
                {order_status}
              </a>
            </Link>
          </li>
          <li>
            <Link href="/contactus">
              <a href="#" onClick={closeNav}>
                <span className="iconify" data-icon="feather:phone-call"></span>
                {contact_us}
              </a>
            </Link>
          </li>
        </ul>

        {renderCurrencies()}
      </div>
    </div>
  );
}

export default MySideNav;
