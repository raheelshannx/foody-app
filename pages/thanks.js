import ThanksImage from "../components/partials/thanks/thanksImage";
import ConfirmMessage from "../components/partials/thanks/confirmMessage";
import OrderCode from "../components/partials/thanks/orderCode";
import NeedHelp from "../components/partials/thanks/needHelp";

export default function Thanks() {
  
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 mt-5 text-center">
          <ThanksImage />
          <ConfirmMessage />
          <OrderCode />
          <hr className="mt-5 mb-5" />
          <NeedHelp />
        </div>
      </div>
    </div>

  );
}