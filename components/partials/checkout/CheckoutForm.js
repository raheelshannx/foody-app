import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import StepOne from "./stepOne";
import StepTwo from "./stepTwo";
import StepThree from "./stepThree";

function CheckoutForm() {
  const checkoutStep = useSelector((state) => state.checkoutStep);
  const [step, setStep] = useState(1);


  useEffect(() => {
    setStep(checkoutStep);
  }, [checkoutStep]);

  const renderStep = () => {
    if (step == 1) {
      return <StepOne />;
    } else if (step == 2) {
      return <StepTwo />;
    } else {
      return <StepThree />;
    }
  };

  return <div>{renderStep()}</div>;
}
export default CheckoutForm;
