import React from "react";
import useTranslation from "../../../services/useTranslation";

function SpecialNotes() {
  const { Notes_delivery } = useTranslation();
  
  return (
    <div className="m-2">
    <h3>Special Notes</h3>
    <textarea
      className="sepcial_Notes"
      placeholder={Notes_delivery}
      rows={4}
      defaultValue={""}
    />
  </div>
  );
}

export default SpecialNotes;
