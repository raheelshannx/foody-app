import React from "react";
import useTranslation from "../../../services/useTranslation";

function SearchArea() {
  const { search_products } = useTranslation();

  return (
    <div className="col-md-12 mt-3">
      <a
        className="form-control"
        data-toggle="modal"
        data-target="#openModal"
        href="#"
      >
        {search_products}
      </a>
    </div>
  );
}

export default SearchArea;