import React from "react";

function ProductImage() {
    return (
        <div>
            <img
                loading="lazy"
                src="/assets/img/item_detail_img.jpg"
                className="w-100"
            />
        </div>
    );
}

export default ProductImage;
