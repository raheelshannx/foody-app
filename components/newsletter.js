import React from "react";
import useTranslation from "../services/useTranslation";
import { Formik } from 'formik';
import networkService from "../services/networkService";
import urlService from "../services/urlService";

function NewsLetter() {
  const { subs_news,sub_new_text,email_news = 'Email for Newsletter',subscribe} = useTranslation();

  const onSubmitApi = async (values) => {
    const response = await networkService.post(urlService.postSubscribeUser,values);
    if (response.IsValid ) {
      let res = response.Payload;
    }
      console.log('response',response);
  }

    return (
          <div className="newsletter mt-4">
            <h2>{subs_news}</h2>
            <p>
              {sub_new_text}
            </p>
            <Formik
                initialValues={{ email: '' }}
                validate={values => {
                  const errors = {};
                  if (!values.email) {
                    errors.email = 'Email is required';
                  } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                  ) {
                    errors.email = 'Invalid email address';
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting ,resetForm}) => {
                  setTimeout(() => {
                    
                    onSubmitApi(values)

                    const data = {
                      email : values.email,
                    }
                    setSubmitting(false);
                  }, 400);
                    resetForm();
                }

              }
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
                }) => (
                    <form className="contactus" onSubmit={handleSubmit}>
                      <div className="form-group">
                        <input
                          maxLength={100}
                          type="text"
                          className="form-control mt-4"
                          placeholder={email_news}
                          name="email"
                          onChange={handleChange}
                          value={values.email}
                        />
                       <p className="text-danger">{errors.email && touched.email && errors.email}</p>
                      </div>
                      <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                        {subscribe }
                       </button>
                      
                      

                    </form>
                  )}
              </Formik>
            
            {/* <Link href="/" passHref>
            </Link> */}
          </div>


    );
}

export default NewsLetter;