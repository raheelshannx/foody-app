import CheckoutHeader from "../components/partials/checkout/checkoutHeader";
import StepWizard from "../components/partials/checkout/stepWizard";
import CheckoutForm from "../components/partials/checkout/checkoutForm";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { setStep } from "../redux/actions/checkoutStepAction";

export default function CheckOut() {
  const webSetting = useSelector((state) => state.webSetting);
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setStep(1));
  });

  return (
    <div>
      <CheckoutHeader />
      <div className="checkout">
        <StepWizard />
        <CheckoutForm />
      </div>
    </div>
  );
}
