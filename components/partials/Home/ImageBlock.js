import React from "react";

function ImageBlock({img}) {
    return (
        <div className="img_block">
            <img loading="lazy" src={img} className="large_img"/>
        </div>
    );
}

export default ImageBlock;
