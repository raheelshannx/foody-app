import React, { useState } from "react";
import Delivery from "./Delivery";
import PickUp from "./PickUp";
import Change from "./Change";
import SearchArea from "./SearchArea";
import { useSelector } from "react-redux";
import PopUp from "./PopUp";
import useLanguage from "../../../services/useLanguage";
const pickupDisplay = (location) => {
    return (
        <span>
            <span
                className="iconify"
                data-inline="false"
                data-icon="carbon:location"
            />{" "}
            {location}
            {/* {holder.outlet.street_1}, {holder.outlet.city},{" "} */}
            {/* {holder.outlet.country} */}
        </span>
    );
}
function SearchFields() {
    const areas = useSelector((state) => state.areas);
    const outlets = useSelector((state) => state.outlets);
    const { pickup, deliver_to } = useLanguage();

    const [location, setLocation] = useState(`${deliver_to == undefined ? '' : deliver_to } / ${pickup == undefined ? '' : pickup}`);
    const [pickupTrue, setPickupTrue] = useState(false);


    return (
        <div>
            <div className="search_area">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="d-flex d-flex2 justify-content-between">
                                {areas.length > 0 ? <Delivery /> : null}
                                {outlets.length > 0 ? <PickUp /> : null}
                            </div>
                        </div>

                        {/* Removed mt-3 from className */}
                        <div className="col-md-12 selected_area">
                            <div className="d-flex p-1 font-bold justify-content-between">
                                {pickupDisplay(location)}
                                <Change pickupTrue={pickupTrue}/>
                            </div>
                        </div>
                        {/* <SearchArea /> */}
                    </div>
                    <SearchArea />
                </div>
            </div>
            <PopUp setLocation={setLocation} setPickupTrue={setPickupTrue}/>
        </div>
    );
}

export default SearchFields;
