import React from "react";
import { useState, useEffect } from "react";
import useCart from "../../../services/useCart";

function ShowQuantity({ product }) {
  const [count, setCount] = useState(0);
  const [increase, setIncrease] = useState(false);
  const [setValue] = useCart({});

  const increment = () => {
    setCount(count + 1);
    setIncrease(true);
  };

  const decrement = () => {
    setCount(count > 0 ? count - 1 : count);
    setIncrease(false);
  };

  useEffect(() => {
    product.addons = [];
    setValue({ quantity: count, product: product, increment: increase });
  }, [count, increase]);

  return (
    <div className="qty_show">
      <form>
        <button
          className="value-button minus"
          id="decrease"
          onClick={decrement}
          value="Decrease Value"
          type="button"
        >
          -
        </button>
        <input
          type="number"
          id="number"
          className="number"
          value={count}
          disabled="disabled"
        />
        <button
          className="value-button"
          id="increase"
          onClick={increment}
          value="Increase Value"
          type="button"
        >
          +
        </button>
      </form>
    </div>
  );
}

export default ShowQuantity;
