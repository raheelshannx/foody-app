import React from "react";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";
import useCart from "../../../services/useCart";
import useTranslation from "../../../services/useTranslation";
import { useState, useEffect } from "react";

function CartListItem() {
  const cart = useSelector((state) => state.cart);
  const [setValue] = useCart({});
  const lang = useLanguage();
  const currency = useCurrency();
  const { empty_cart } = useTranslation();

  const [count, setCount] = useState(0);
  const [increase, setIncrease] = useState(false);

  const removeItem = (product) => {
    setValue({ quantity: 0, product: product, increment: false });
  };
  const increment = (product) => {
    setCount(count + 1);
    setIncrease(true);
    setValue({ quantity: count, product: product, increment: increase });
  };

  const decrement = (product) => {
    setCount(count > 0 ? count - 1 : count);
    setIncrease(false);
    setValue({ quantity: count, product: product, increment: increase });
  };
  return (
    <ul className="cart_list_items pt-2 pb-2">
      {cart.items.map((product) => {
        return (
          <li key={product.variant_id} className="d-flex flex-column">
            <div>
              <span className="">{product[lang].name}</span>
            </div>
            <div className="d-flex justify-content-between">
              <div className="qty_show" style={{ width: "auto" }}>
              <form>
                <button
                  className="value-button minus"
                  id="decrease"
                  value="Decrease Value"
                  type="button"
                  onClick={() => decrement(product)}
                >
                  -
                </button>
                <input
                  type="number"
                  id="number"
                  className="number"
                  disabled=""
                  defaultValue={product.quantity}
                />
                <button
                  className="value-button"
                  id="increase"
                  value="Increase Value"
                  type="button"
                  onClick={() => increment(product)}
                >
                  +
                </button>
                </form>
              </div>
              <strong style={{ "paddingTop": "11px" }}>
                {currency} : {product.total}
              </strong>
              <span className="x_closer" onClick={() => removeItem(product)} style={{ "paddingTop": "7px" }}>
                X
              </span>
            </div>
          </li>
        );
      })}
    </ul>
  );
}
export default CartListItem;
