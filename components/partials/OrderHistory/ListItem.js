import React from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";
import Link from "next/link";

function ListItem({ order }) {
    const {name, order_id } = useTranslation();
    const store = useSelector((state) => state.store);
    const iconClass = order.status == 'Complete' ? 'check-circle-filled' : 
    order.status == 'Failed' ? 'round-cancel' : 
    order.status == 'Confirmed' ? 'check-circle-filled' : 'check-circle-filled2';
    return (
        order != null ?
            <Link href={{ pathname: '/orderdetail', query: { id: order.id } }}>
                <li>
                  <span className="item_title">
                    <span
                      className="iconify mr-1 check-circle-filled"
                      data-icon="ant-design:check-circle-filled"
                    />{" "} {order['order_items'] != null ? order['order_items'][0].description : '' } </span
                  >{" "} <span className="qqty">x{order['order_items'].length}</span>{" "}
                  <strong className="d-block pt-1 pb-1">{store.currency} : {order.total}</strong>
                  <span className="order-id">{order_id} : {order.order_number}</span>{" "}
                  <span className="timendate pull-right">{order.created_at}</span>
                </li>
            </Link> 
        : ''
    );
}

export default ListItem;