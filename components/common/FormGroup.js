import React from "react";

function FormGroup(props) {
    return (
        <div className="form-group">
            <label className="control-label">
                {props.label} <em>*</em>
            </label>
            <input
                maxLength={100}
                name={props.name}
                value={props.value}
                onChange={props.onChange}
                onBlur={props.onBlur}
                type={props.type!=null? props.type : "text"}
                className="form-control"
                placeholder={props.hint}
            />
        </div>
    );
}

export default FormGroup;
