function ContinueButton(props){
    return(
        <button
        className="btn btn-primary nextBtn btn-lg pull-right"
        type="submit"
      >
        {props.title}
      </button>
    );
}
export default ContinueButton;