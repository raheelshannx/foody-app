import React from "react";

function ItemDetail() {
    return (
        <div className="item_detail">
            <h3>Aloo Chicken Tikka With Masala </h3>
            <p className="price">
                <span>KD 30:00</span>
                <small>KD 20:00</small>
            </p>
        </div>
    );
}

export default ItemDetail;
