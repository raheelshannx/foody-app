import React from "react";
import CartDetailArea from "./partials/Home/cartDetailArea";
import WhatsappIcon from "./partials/Home/whatsappIcon";
import { useSelector } from "react-redux";

function FixedButtonArea() {
  const settings = useSelector((state) => state.webSetting);

  const whatsApp = (settings) => {
    if (settings.whatsapp_number != null) {
      return <WhatsappIcon number={settings.whatsapp_number} />;
    } else {
      return "";
    }
  };

  return (
    <div className="btn_area">
      <CartDetailArea />
      {whatsApp(settings)}
    </div>
  );
}

export default FixedButtonArea;
