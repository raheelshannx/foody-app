import React from "react";
import Products from "./products";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage"

function CategoriesArea({ products }) {

  const categories = useSelector((state) => state.categories);
  const lang = useLanguage();

  return (
    <div className="products_area">
      <div id="accordion" className="accordion  mt-4 mb-4">
        <div className="card  mb-0">
          {categories.map((category,index) => {

            let filteredProducts = products.filter(prod => {
              return prod.category_id == category.id;
            });

            let hash = `#collapseOne${category.id}`;
            let id = `collapseOne${category.id}`;

            if(filteredProducts!=null && filteredProducts.length>0){
            return (
              <div className="panel" key={category.id}>
                <div
                  className="card-header collapsed mb-2"
                  data-toggle="collapse"
                  href={hash}
                >
                  <a className="card-title">
                    { category[lang] }<span>( {filteredProducts.length} )</span>
                  </a>
                </div>
                <div
                  id={id}
                  className="card-body collapse "
                  data-parent="#accordion"
                >
                  {filteredProducts.map((product) => {
                    return <Products product={product} key={product.product_id} />;
                  })}
                </div>
              </div>
            );
                }
          })}
        </div>
      </div>
    </div>
  );
}

export default CategoriesArea;
