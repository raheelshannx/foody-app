import React from "react";
import SubHeader from "../components/common/SubHeader";
import ProductGridItem from "../components/partials/Home/ProductGridItem";
import PopUp from "../components/partials/Home/PopUp";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import useLanguage from "../services/useLanguage";
import { useRouter } from 'next/router'

function Home() {
  const router = useRouter();
  const { cat,products } = router.query;

  const category = JSON.parse(cat);
  const prods = JSON.parse(products);

  let filteredProducts = prods.filter(prod => {
    return prod.category_id == category.id;
  });
  const lang = useLanguage();
  return (
    <div>
      <SubHeader title={category[lang]}/>
      <div className="home2_products_area">
        <div className="products_row">
          <div className="product_scroller_3">
            <ul className="inner_pro">
              {
                filteredProducts.map(product => {
                  return (
                    <ProductGridItem data={product} />
                  )
                })
              }

            </ul>
          </div>
        </div>
      </div>
      <PopUp />
      <DetailPopUp />
      <FixedButtonArea />
    </div>

  );
}

export default Home;
