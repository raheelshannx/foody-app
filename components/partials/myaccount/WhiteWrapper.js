import ListItem from "./listItem";
import useTranslation from "../../../services/useTranslation";

function WhiteWrapper(){
  const {edit_account , order_history , address_book , change_password} = useTranslation();
    return(
        <div className="white_wrapper cart">
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <ul className="ul-account-list pt-3">
            <ListItem link="/editaccount" dataicon="bx:bx-user" title= {edit_account}/>
              <ListItem link="/orderhistory" dataicon="carbon:list-checked" title= {order_history}/>
              <ListItem link="/addressbook" dataicon="carbon:location" title= {address_book}/>
              <ListItem link="changepassword" dataicon="bytesize:lock" title= {change_password}/>
          </ul>
        </div>
      </div>
    </div>
  </div>
    );
}
export default WhiteWrapper;