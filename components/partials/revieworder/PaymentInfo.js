import React from "react";
import useTranslation from "../../../services/useTranslation";

function PaymentInfo() {
    const {Payment_Info} = useTranslation();
    return (
        <div>
            <h2 className="text-uppercase pt-4 pb-3"> {Payment_Info} </h2>
            <p>
                <span
                    className="iconify checked_icon"
                    data-icon="ant-design:check-circle-filled"
                />
                MyFatoorah
          </p>
            <hr />
        </div>
    );
}

export default PaymentInfo;
