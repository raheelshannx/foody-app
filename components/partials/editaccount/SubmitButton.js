import useTranslation from "../../../services/useTranslation";
function SubmitButton() {
    const { Update } = useTranslation();
    return (
        <button className="primary mt-3 mb-3">{Update}</button>
    );
}
export default SubmitButton;