import FormGroup from "../../common/FormGroup";
import RoundedButton from "../../common/RoundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import store from "../../../redux/index";
import { useRouter } from "next/router";
import React, { useEffect,useState } from "react";


function WhiteWrapper() {
    const {success, name, phone, email, area_name, block, street_number, city, country, add_address } = useTranslation();
    const reduxStore = store;
    let [data, setdata] = useState();
    let [init, setInit] = useState(false);
    const router = useRouter();
    const { id } = router.query;

    
    const initialize = async () => {
        if(id != null){
            const response = await networkService.get(urlService.getAddressEdit+id);
            if (response.IsValid) {
             setdata(response.Payload);
             setInit(true);
            }
        }
    }
    useEffect(() => {
      if(!init){
        initialize();
      }
    },[data]);
          console.log('addresses',data);

    const onSubmitApi = async (values) => {

      let address = 
          values.street_number + ', ' + 
          values.block + ', ' + 
          values.area_name + ', ' + 
          values.city + ', ' + 
          values.country;

        let data = {
            address: address,
            address_id: values.id,
            name: values.name,
            phone: values.phone,
            email: values.email,
            area_name: values.area_name,
            block: values.block,
            street_number: values.street_number,
            city: values.city,
            country: values.country,
          };

      console.log('user',values);
      const response = await networkService.post(urlService.postAddressUpdate,data);
      if (response != null && response.IsValid) {
        toastr.success(success);
        setTimeout(() => {
          const win = window.location.replace('addressbook');
        }, 800);
      }
    }
   
    return (
        data != null ?
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <br />
                        <Formik
                            initialValues={{
                                id:data.id, 
                                name:data.name, 
                                phone: data.phone, 
                                email:data.email , 
                                area_name: data.area_name, 
                                block: data.block, 
                                street_number: data.street_number, 
                                city: data.city, 
                                country: data.country 
                            }}
                            validate={values => {
                                const errors = {};
                                if (!values.area_name) {
                                    errors.area_name = 'Area Name is required';
                                }
                                if (!values.block) {
                                    errors.block = 'Block is required';
                                }
                                if (!values.street_number) {
                                    errors.street_number = 'Street Number is required';
                                }
                                if (!values.city) {
                                    errors.city = 'City is required';
                                }
                                if (!values.country) {
                                    errors.country = 'Country is required';
                                }
                                return errors;
                            }}
                            onSubmit={(values, { setSubmitting }) => {
                                setTimeout(() => {
                                    onSubmitApi(values);
                                    // alert(JSON.stringify(values, null, 2));
                                    setSubmitting(false);
                                }, 400);
                            }}
                        >
                            {({
                                values,
                                errors,
                                touched,
                                handleChange,
                                handleBlur,
                                handleSubmit,
                                isSubmitting,
                                /* and other goodies */
                            }) => (
                                    <form onSubmit={handleSubmit}>
                                        <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                                        <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                                        <FormGroup label={phone} name="phone" onChange={handleChange} onBlur={handleBlur} value={values.phone} hint={phone} />
                                        <p className="text-danger">{errors.phone && touched.phone && errors.phone}</p>

                                        <FormGroup label={email} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email} />
                                        <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                                        <FormGroup label={area_name} name="area_name" onChange={handleChange} onBlur={handleBlur} value={values.area_name} hint={area_name} />
                                        <p className="text-danger">{errors.area_name && touched.area_name && errors.area_name}</p>

                                        <FormGroup label={block} name="block" onChange={handleChange} onBlur={handleBlur} value={values.block} hint={block} />
                                        <p className="text-danger">{errors.block && touched.block && errors.block}</p>

                                        <FormGroup label={street_number} name="street_number" onChange={handleChange} onBlur={handleBlur} value={values.street_number} hint={street_number} />
                                        <p className="text-danger">{errors.street_number && touched.street_number && errors.street_number}</p>

                                        <FormGroup label={city} name="city" onChange={handleChange} onBlur={handleBlur} value={values.city} hint={city} />
                                        <p className="text-danger">{errors.city && touched.city && errors.city}</p>

                                        <FormGroup label={country} name="country" onChange={handleChange} onBlur={handleBlur} value={values.country} hint={country} />
                                        <p className="text-danger">{errors.country && touched.country && errors.country}</p>

                                        <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                                            {add_address}
                                        </button>
                                    </form>
                                )}
                        </Formik>
                    </div>
                </div>
            </div>
        </div>
        : ""
    );
}
export default WhiteWrapper;