import React from "react";
import useTranslation from "../../../services/useTranslation";

import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router"; 

function OrderCode() {
    const {ORDER_CODE} =  useTranslation();
	
	const router = useRouter();
  	const { order_number } = router.query;

    return (
        <h3 className="mt-2">
            {ORDER_CODE} <b>{order_number}</b>
        </h3>
    );
}

export default OrderCode;
