import React from "react";

function ProductDescription() {
    return (
        <div>
            <p className="p-2 pl-4 pr-4">
                Beef tenders cubes tossed gravy with homemade spices, served with 1
                naan bread and 1 side order.
    </p>
        </div>
    );
}

export default ProductDescription;
