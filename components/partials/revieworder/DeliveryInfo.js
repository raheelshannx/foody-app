import React from "react";

function DeliveryInfo(props) {
    return (
        <div class="mt-3">
            <h6>{props.title}</h6>
            <p>{props.info}</p>
            <hr />
        </div>
    );
}

export default DeliveryInfo;
