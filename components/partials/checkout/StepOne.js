import CheckoutInfo from "./checkoutInfo";
import FormGroup from "../../common/formGroup";
import ContinueButton from "./continueButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { setStep } from "../../../redux/actions/checkoutStepAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";
import { useEffect, useState } from "react";

function StepOne() {
  const { name, phone, email , required , invalid_email} = useTranslation();
  const [username, setUsername] = useState();
  const [useremail, setUseremail] = useState();
  const [userphone, setUserphone] = useState();
  const dispatch = useDispatch();
  const [holder, setHolder] = useCheckoutDataHolder();
  const proceed = "Proceed";

  useEffect(() => {

    holder.name = username;
    holder.email = useremail;
    holder.phone = userphone;

    setHolder(holder);

    console.log('test');

    setInitialize(true);
  }, [username, useremail, userphone]);

  const setInitialize = async () => {
    const response = await networkService.get(urlService.getUser);

    if (response != null) {

      setUsername(response.name);
      setUseremail(response.email);
      setUserphone(response.mobile);
      console.log('set');
    }
  };

  return (
    <div className="row setup-content" id="step-1">
      <div className="col-xs-6 ">
        <div className="col-md-12">
          <CheckoutInfo title="Contact Information" subTitle="" />
          <Formik
          enableReinitialize
            initialValues={{ name: username, email: useremail, phone: userphone }}
            validate={(values) => {
              const errors = {};
              if (!values.email) {
                errors.email = required;
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = invalid_email;
              }
              if (!values.name) {
                errors.name = required;
              }
              if (!values.phone) {
                errors.phone = required;
              }

              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setTimeout(() => {
                setSubmitting(false);

                holder.name = values.name;
                holder.email = values.email;
                holder.phone = values.phone;

                setHolder(holder);

                dispatch(setStep(2));
              }, 400);
              // onSubmitApi(values)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => (
              <form className="contactus" onSubmit={handleSubmit}>
                <FormGroup
                  label={name}
                  name="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.name}
                  hint={name}
                />
                <p className="text-danger">
                  {errors.name && touched.name && errors.name}
                </p>

                <FormGroup
                  label={phone}
                  name="phone"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                  hint={phone}
                />
                <p className="text-danger">
                  {errors.phone && touched.phone && errors.phone}
                </p>

                <FormGroup
                  label={email}
                  name="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                  hint={email}
                />
                <p className="text-danger">
                  {errors.email && touched.email && errors.email}
                </p>

                <button
                  className="btn btn-primary nextBtn btn-lg pull-right"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {proceed}
                </button>
              </form>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepOne;
