import React from "react";
import VendorLogo from "./vendorLogo";
import VendorDescription from "./vendorDescription";

function VendorDetail() {
    return (
        <div className="vendor p-3">
            <div className="row align-items-center">
                <VendorLogo />
                <VendorDescription />
            </div>
        </div>
    );
}

export default VendorDetail;
