import React from "react";
import useTranslation from "../../../services/useTranslation";

function Delivery() {
  const { Delivery } = useTranslation();
  const deliveryPopUp=()=>{
    jQuery('#pills-profile-tab').removeClass('active');
    jQuery("#pills-profile").removeClass('active show');

    jQuery('#pills-home-tab').addClass('active');
    jQuery('#pills-home').addClass('active show');

  }
  return (
    <div className="custom-control custom-radio">
      <input
        type="radio"
        id="customRadio1"
        name="pickup"
        className="custom-control-input delivery_radio"
        onClick={deliveryPopUp}
        defaultChecked
      />
      <label
        className="custom-control-label"
        htmlFor="customRadio1"
        data-toggle="modal"
        data-target="#pickup_opup"
        htmlFor="customRadio1">
        {Delivery}

      </label>
    </div>

  );
}

export default Delivery;
