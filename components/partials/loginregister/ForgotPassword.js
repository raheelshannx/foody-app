import React from "react";
import useTranslation from "../../../services/useTranslation";
import { Formik } from "formik";
import axios from "axios";
import qs from "qs";
import Router from "next/router";
import FormGroup from "../../common/FormGroup";
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";

function ForgotPassword() {
    const {success, forgot_password,submit,your_email_address, email,email_address ,invalid_email} = useTranslation();
	const required = "Email is required";

	const onSubmitApi = async (values) => {
		let data = {
		  email: values.email,
		};
        const response = await networkService.get(urlService.getForgotPassword,data);
        if (response.IsValid) {
            toastr.success(success);
	        setTimeout(() => {
	          const win = window.location.replace('loginregister');
	        }, 800);
        }else{
        	// toastr.error(Invalid_email_password);
            toastr.error(response.Errors);
        }
        console.log(response);

      }
    return (
		<div>
		    <a href="#" data-toggle="modal" data-target="#exampleModalCenter">
		        {forgot_password}
		    </a>

		    <div className="modal fade" id="exampleModalCenter" tabindex="{-1}" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		        <div className="modal-dialog modal-dialog-centered" role="document">
		            <div className="modal-content resetpass">
		                <div className="modal-body">
		                    <h5 className="modal-title mb-1" id="exampleModalLongTitle">
		                        Forgot password
		                    </h5>
		                    <Formik
					        initialValues={{ email: "" }}
					        validate={(values) => {
					          const errors = {};
					          if (!values.email) {
					            errors.email = required;
					          } else if (
					            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
					          ) {
					            errors.email = invalid_email;
					          }
					          
					          return errors;
					        }}
					        onSubmit={(values, { setSubmitting }) => {
					          setTimeout(() => {
					          	onSubmitApi(values);
                        		setSubmitting(false);
					            
					          }, 400);
					        }}
					      >
					        {({
					          values,
					          errors,
					          touched,
					          handleChange,
					          handleBlur,
					          handleSubmit,
					          isSubmitting,
					        }) => (
		                    <form onSubmit={handleSubmit}>
		                    	<FormGroup label={email_address} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email_address} />
                            	<p className="text-danger">{errors.email && touched.email && errors.email}</p>

			                    <button className="primary mt-3 mb-3" type="submit"   disabled={isSubmitting}>
	                                {submit}
	                            </button>
		                    </form>
		                    )}
      					</Formik>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

    );
}

export default ForgotPassword;
