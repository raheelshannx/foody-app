import React from "react";
import useTranslation from "../../../services/useTranslation";

function SearchCode() {
    const { search } = new useTranslation();
    return (
        <input
            type="text"
            placeholder={search}
            className="form-control"
            name
        />
    );
}

export default SearchCode;
