import React from "react";
import useTranslation from "../../../services/useTranslation";
function SubmitButton() {
    const { change_password } = useTranslation();
    return (
        <button className="primary mt-3 mb-3">{change_password}</button>
    );
}
export default SubmitButton;