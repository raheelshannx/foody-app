//set into list action
export const addCurrency = (payload) => {
    return {
        type: 'ADD_CURRENCY',
        payload
    }
}

export function setDefault(payload) {
    return {
        type: 'SET_CURRENCY',
        payload
    }
};