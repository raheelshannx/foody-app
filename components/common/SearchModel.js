import React, { useState, useEffect } from "react";
import SearchBox from "./SearchBox";
import CategoriesArea from "../partials/Home/CategoriesArea";
import Products from "../partials/Home/Products";
import CloseButton from "../partials/Home/CloseButton";
import useTranslation from "../../services/useTranslation";
import useLanguage from "../../services/useLanguage";
export default function SearchModel({ products }) {
  const [filteredProducts, setFilteredProducts] = useState([]);
  const { search_products } = useTranslation();
  const lang = useLanguage();

  const handleChange = (event) => {
    console.log(event.target.value);
    if (event.target.value != '') {
      let prods = [];
      products.map(p => {
         
          if(p[lang].name.toLowerCase().includes(event.target.value)){
            prods.push(p)
          }

         return p;
      });
      setFilteredProducts(prods);
    }
  };

  useEffect(() => {}, [filteredProducts]);

  return (
    <div
      className="modal fade"
      id="openModal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header ">
            <div className="col-md-12 mt-3">
              <input
                type="text"
                placeholder={search_products}
                className="form-control"
                onChange={() => handleChange(event)}
              />
            </div>
            <button
              type="button"
              className="btn btn-dark"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <div className="panel modal-body ">
            {filteredProducts.map((product) => {
              return (
                <div className="card-body" key={product.product_id} >
                  <Products product={product}/>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
