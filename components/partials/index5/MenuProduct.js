import React from "react";
import useLanguage from "../../../services/useLanguage";
import Link from "next/link";

function MenuProduct({ data, products }) {
  const lang = useLanguage();
  return (
    <div>
      <li>
      <Link href={{ pathname: '/index5_list', query: { cat: JSON.stringify(data),products:JSON.stringify(products)} }}>
          <a href="#">
            <div className="img_block">
              <h4>{data[lang]}</h4>
              <span>{products.length}</span>
              <img loading="lazy" src={data.image} className='xlarge_img' />
            </div>
          </a>
        </Link>
      </li>
    </div>
  );
}

export default MenuProduct;
