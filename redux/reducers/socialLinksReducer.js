const categoryItem = (state, action) => {
  switch (action.type) {
      case 'SET_SOCIALLINKS':
          return action.payload;
      default:
          return state;
  }
}

export default function socialLinksReducer(state = [], action) {
  switch (action.type) {
      case 'SET_SOCIALLINKS':
          let item = state.find(item =>  item.link == action.payload.link);
          if (item == undefined) {
              return [
                  ...state,
                  categoryItem(undefined, action)
              ];
          } else {
              return state;
          }
      default:
          return state;
  }
}