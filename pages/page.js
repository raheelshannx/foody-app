import useLanguage from "../services/useLanguage";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ReactHtmlParser from "react-html-parser";

export default function Page() {
  const lang = useLanguage();
  const router = useRouter();
  const [page, setPage] = useState();
  const { slug } = router.query;

  const getPage = async (slug) => {
    let url = urlService.getPage + "/" + slug;
    const response = await networkService.get(url);

    if (response != undefined && response.IsValid) {
      setPage(response.Payload);
    }
  };

  useEffect(() => {
    getPage(slug);
  }, [slug]);

  const renderPage = () => {
    if (page != undefined) {
      return (
        <div>
          <h1>{page[lang].name}</h1>
          <p>{page[lang].name}</p>
          {ReactHtmlParser(page[lang].description)}
        </div>
      );
    } else {
      <div></div>;
    }
  };

  return <div>{renderPage()}</div>;
}
