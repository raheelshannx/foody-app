import React from "react";

function ItemImage() {
    return (
        <div className="col-md-4">
            <span className="item_img">
                <img
                    loading="lazy"
                    src="/assets/img/chicken.jpg"
                    className="w-100"
                />
            </span>
        </div>
    );
}

export default ItemImage;
