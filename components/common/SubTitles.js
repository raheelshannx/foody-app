import React from "react";

function SubTitles(props) {
  return (
  <h3 className="mt-4 mb-2">{props.title}</h3>
  );
}

export default SubTitles;
