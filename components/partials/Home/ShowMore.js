import React from "react";
import { setPopup } from "../../../redux/actions/popupAction";
import useLanguage from "../../../services/useLanguage";
import { useDispatch } from "react-redux";

function ShowMore({product}) {
    const showmore ="Show More";
    const lang = useLanguage();
    const dispatch = useDispatch();
    const setPopupData = () => {
        product[lang].addons.map((addon) => {
          addon.items.map((item) => {
            item.selected = false;
            return item;
          });
          addon.complete = false;
          return addon;
        });
    
        dispatch(setPopup(product));
      };
    
    return (
        <h4 data-toggle="modal" data-target="#item_detail_popup" onClick={setPopupData}>
            {showmore}
</h4>
    );
}

export default ShowMore;
