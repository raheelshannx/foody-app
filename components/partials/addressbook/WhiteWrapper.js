import React, { useEffect,useState } from "react";
import AddressList from "./AddressList";
import RoundedButton from "../../common/RoundedButton";
import Link from "next/link";
import useTranslation from "../../../services/useTranslation";
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import store from "../../../redux/index";
const reduxStore = store;
        

function WhiteWrapper() {

    const {address ,success, add_address,name, phone, edit} = useTranslation();
    const {Delete} = 'Delete';
    const [holder, setHolder] = useCheckoutDataHolder();
    let [data, setdata] = useState();
    let [init, setInit] = useState(false);
    // let data = [];

      const initialize = async () => {
        const response = await networkService.get(urlService.getAddressIndex);
        if (response.IsValid) {
         setdata(response.Payload);
         setInit(true);

          // console.log('addresses',data);
        }
        // console.log(response);
      }
      const deleteAddress = async (id) => {
          let i = { 'id' : id}
        const response = await networkService.get(urlService.getAddressDelete,i);
        if (response.IsValid) {
            initialize();
            toastr.success(success);
            
        }
        // console.log(response);
      }
      const editAddress = async (id) => {
          window.location.replace('editaddress?id='+id);
      }

      useEffect(() => {
          if(!init){
            initialize();
          }
      },[data]);
      const renderAddress = () => {
          if(data != null && data.length > 0){
              return (
                 data.map(addr => {
                     return(
                        <li >
                            <ul>
                                <li>
                                    {name} : <span>{addr.name}</span>{" "}
                                    <span className="pull-right">Home</span>
                                </li>
                                <li>{phone} :{addr.phone}</li>
                                <li>{address} : {addr.address}</li>
                                <li>
                                    <a className="edit-btn" href onClick={() => editAddress(addr.id)}>
                                        {edit}
                                    </a>
                                    <a className="delete-btn ml-3" href  onClick={() => deleteAddress(addr.id)}>
                                        Delete
                                    </a>
                                </li>
                            </ul>
                        </li>
                     )
                }) 
              );
          }
      };


    return (
        <div className="white_wrapper cart">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <ul className="ul-account-list ul-address-book-list  pt-3">
                            {renderAddress()}
                        </ul>
                        <roundedButton title={add_address} link="/addaddress" />
                    </div>
                </div>
            </div>
        </div>
    );
}
export default WhiteWrapper;