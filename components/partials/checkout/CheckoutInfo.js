import React from 'react';
function CheckoutInfo(props) {
    return (
        <div>
            <h3 className="mt-4 mb-2">{props.title}</h3>
            <p>{props.subTitle}</p>
        </div>
    );
}
export default CheckoutInfo;