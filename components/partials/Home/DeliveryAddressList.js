import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setPickup } from "../../../redux/actions/pickUpAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useTranslation from "../../../services/useTranslation";

function DeliveryAddressList({setLocation,setPickupTrue}) {
  const areas = useSelector((state) => state.areas);
  const [selected, setSelected] = useState(0);
  const dispatch = useDispatch();
  const [holder, setHolder] = useCheckoutDataHolder();
  const {deliver_to} = useTranslation();

  const onChanged = (val) => {
    setSelected(val);

    holder.pickup = false;
    holder.outlet = null;
    holder.area = val;

    setHolder(holder);
    setPickupTrue(false);
    setLocation(deliver_to+" "+val.name);

    // dispatch(
    //   setPickup({
    //     pickup: false,
    //     outlet: "",
    //     area: val,
    //   })
    // );
    jQuery(".selected_area").show();
  };
  return (
    areas.length>0 &&
    <div
      className="tab-pane fade"
      id="pills-home"
      role="tabpanel"
      aria-labelledby="pills-home-tab"
    >
      {/* Delivery address list*/}
      <ul className="pickup_list">
        {areas.map((area) => {
          let id = `area-${area.id}`;
          return (
            <li key={area.id}>
              <h3><label htmlFor={id}>{area.name}</label></h3>
              <span className="select_a">
                <div className="custom-control custom-radio">
                  <input
                    type="radio"
                    id={id}
                    name="customRadio"
                    value={area}
                    checked={area == selected}
                    onChange={() => onChanged(area)}
                    className="custom-control-input"
                    data-dismiss="modal"
                  />
                  <label className="custom-control-label" htmlFor={id} />
                </div>
              </span>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default DeliveryAddressList;
