import { useSelector } from "react-redux";

function useLanguage() {
  const languages = useSelector((state) => state.languages);
  const filtered = languages.filter((item) => item.isDefault);
  const currentLanguage = filtered[0];
  return currentLanguage.short_name;
}

export default useLanguage;
