import ProductItem from "./productItem";
import { useRouter } from "next/router";

function WhiteWrapper({products}){
    return(
        <div className="white_wrapper products_list2 ">
    <div className="container">
        {
            products.map(product=>{
                return <ProductItem key={product.id} product={product}/>
            })
        }
    </div>
  </div>
    );
}
export default WhiteWrapper;