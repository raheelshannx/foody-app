import React from "react";
import { useSelector } from "react-redux";

function CartItem({item}) {
	const store = useSelector((state) => state.store);
	console.log('item',item);
    return (
        <div className="d-flex p-3 mt-2  justify-content-between align-items-center">
            <img loading="lazy" src={item.variant.product != null ? item.variant.product.images != null ?
             item.variant.product.images[0].url :'assets/img/image-not-available.jpg' : 'assets/img/image-not-available.jpg' } width={50} />
            <p className="mb-0">{item.description}</p>
            <strong>{store.currency} : {item.total}</strong>
        </div>
    );
}

export default CartItem;