import React from "react";
import useTranslation from "../../../services/useTranslation";


function ConfirmMessage() {
    const { Your_successfully } = useTranslation();
    return (
        <h3 className="mt-4"> 
        {Your_successfully}
        </h3>
    );
}

export default ConfirmMessage;
