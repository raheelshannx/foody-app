import React, { useState, useEffect } from "react";

import ProductGridItem from "../Home/productGridItem";
import TopCategory from "../Home/topCategory";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import Link from "next/link";
import FilteredProductSection from "./filteredProductSection";

function ProductSection({ products }) {
  const categories = useSelector((state) => state.categories);
  const lang = useLanguage();
  

  const { showmore } = useTranslation();

  return (
    <div>
      {categories.map((category) => {
        
        return (
          <div key={`cat-sub-` + category.id}>
            <div className="d-flex mt-3 ml-3 mr-3  font-bold justify-content-between">
              <h6>{category[lang]}</h6>
              <span>
                <Link
                  href={{
                    pathname: "/moreitems",
                    query: {
                      data: JSON.stringify(products),
                      categoryName: category[lang],
                    },
                  }}
                >
                  <a href="#" className="show-more">
                    {showmore}
                  </a>
                </Link>
              </span>
            </div>
            <FilteredProductSection
              category={category}
              products={products}
              key={category.id}
            />
          </div>
        );
      })}
    </div>
  );
}

export default ProductSection;
