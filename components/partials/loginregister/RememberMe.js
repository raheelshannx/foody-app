import React from "react";
import useTranslation from "../../../services/useTranslation";

function RememberMe() {
    const {remember_me} = useTranslation();
    return (
        <div className="custom-control custom-checkbox">
            <input
                type="checkbox"
                className="custom-control-input"
                id="customCheck"
                name="example1"
            />
            <label
                className="custom-control-label"
                htmlFor="customCheck"
            >
                {remember_me}
      </label>
        </div>
    );
}

export default RememberMe;
