import React from "react";

function TextArea(props) {
    return (
        <div className="form-group">
            <label className="control-label">
                {props.label} <em>*</em>
            </label>
            <textarea
                className="form-control"
                placeholder={props.hint}
                rows={3}
                name={props.name}
                value={props.value}
                onChange={props.onChange}
                onBlur={props.onBlur}
                type="text"
                required={props.required}
            />
        </div>
    );
}

export default TextArea;