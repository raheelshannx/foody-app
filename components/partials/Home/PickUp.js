import React from "react";
import useTranslation from "../../../services/useTranslation";
function PickUp() {
    const { pickup } = new useTranslation();
    const pickupPopUP=()=>{
        jQuery('#pills-home-tab').removeClass('active');
        jQuery('#pills-home').removeClass('active show');

        jQuery("#pills-profile-tab").addClass('active');
        jQuery("#pills-profile").addClass('active show');
      }
    return (
        <div className="custom-control custom-radio">
            <input
                type="radio"
                id="customRadio2"
                name="pickup"
                onChange={pickupPopUP}
                className="custom-control-input pickup_radio"
            />
            <label
                className="custom-control-label"
                htmlFor="customRadio2"
                data-toggle="modal"
                data-target="#pickup_opup"
            >
                {pickup}
              </label>
        </div>
    );
}

export default PickUp;
