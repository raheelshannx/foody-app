import { useDispatch, useSelector } from "react-redux";
import { setDefault } from "../../redux/actions/languageAction";
import { useEffect } from "react";

function LanguageDropDown() {
  const languages = useSelector((state) => state.languages);
  const dispatch = useDispatch();
  const filtered = languages.filter((item) => item.isDefault);
  const currentLanguage = filtered[0];
  const handleChange = (l) => {
    const lang = JSON.parse(l);
    if (currentLanguage.short_name != lang.short_name) {
      dispatch(setDefault(lang));
    }
  };
  useEffect(() => {}, [languages]);
  return (
    <span className="languages">
      <div className="dropdown">
        <span
          className="lang dropdown-toggle"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {currentLanguage.symbol}
        </span>

        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          {languages.map((language) => {
            return (
              <a
                className="dropdown-item"
                key={language.short_name}
                href="#"
                onClick={() => handleChange(JSON.stringify(language))}
              >
                {language.symbol}
              </a>
            );
          })}
        </div>
      </div>
    </span>
  );
}
export default LanguageDropDown;
