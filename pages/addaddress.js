import React from "react";
import subHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/addaddress/whiteWrapper";
import useTranslation from "../services/useTranslation";

export default function AddAddress() {
  const { add_address } = useTranslation();
  return (
    <div>
      <SubHeader title={add_address}/>
      <WhiteWrapper />
    </div>
  );
}