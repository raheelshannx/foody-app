import OrderStatus from "./orderStatus";
import OrderList from "./orderListItem";
import CartItem from "./cartItem";
import CartTotal from "./cartTotal";
import SubmitButton from "./submitButton";
import useTranslation from "../../../services/useTranslation";
import { useEffect, useState } from "react";
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import { useRouter } from "next/router"; 

function WhiteWrapper({ order_id }) {
  const { delivered_to, payment_method, Order_Items } = useTranslation();

  let [order, setOrder] = useState();
  let [init, setInit] = useState(false);
  const router = useRouter();
  const { id } = router.query;

  const initialize = async () => {
    const response = await networkService.get(urlService.getOrderList+ id);
    if (response.IsValid) {      
      setOrder(response.Payload);
      setInit(true);
    console.log(response);
    }
  }
  useEffect(() => {
    if(!init){
      initialize();
    }
  },[order]);


  return (
    order != null ?
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ul className="ul-order-detail-list pt-3">
              <OrderStatus order_no={order.order_number} status={order.status} time={order.created_at} />
              

              <OrderList title={delivered_to} detail={order.address.address != ""  ? order.address.address : 
                  order.address.avanue != undefined ? order.address.avanue : + ', ' + 
                  order.address.house_detail + ', ' + 
                  order.address.street + ', ' + 
                  order.address.block + ', ' + 
                  order.address.area_name + ', ' + 
                  order.address.city + ', ' + 
                  order.address.country

               } />


              <OrderList title={payment_method} detail={order.payment_method != null ? order.payment_method.name : ''} />
            </ul>
            <hr />
            <div className="item-box pt-3">
              <h3>{Order_Items}</h3>
              {order['order_items'] != null ? order['order_items'].map(item => {
                return (
                <CartItem item={item} />
                );
              }) : ''}
            </div>
            <hr />
            <CartTotal order={order} />
            <SubmitButton />
          </div>
        </div>
      </div>
    </div>
    :''

  );
}
export default WhiteWrapper;