import React from "react";
import useTranslation from "../../../services/useTranslation";
function CheckoutHeader() {
  const { check_out } = useTranslation();
    return (
        <div className="checkout_header">
    <h1 className="text-center">{check_out}</h1>
  </div>
    );
}
export default CheckoutHeader;