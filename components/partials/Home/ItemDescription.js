import React from "react";
import useCurrency from "../../../services/useCurrency";
import { useSelector} from "react-redux";

function ItemDescription({ name, price }) {
    const currency = useCurrency();
    const store = useSelector((state) => state.store);
    return (
        <div className="item_d">
            <p>{name}</p>
            <p>{`${currency} ${parseFloat(price).toFixed(store.round_off)}`}</p>
        </div>
    );
}

export default ItemDescription;
