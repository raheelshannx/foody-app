import React from "react";

function ThanksImage() {
    return (
        <img loading="lazy" src="assets/img/thanks_icon.svg" />
    );
}

export default ThanksImage;
