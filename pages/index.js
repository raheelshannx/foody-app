import Slider from "../components/partials/Home/Slider";
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";

import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import useSetup from "../services/useSetup";
import { useState, useEffect } from "react";

import Home3Products from "../components/partials/Home/Home3Products";
import Home4Products from "../components/partials/Home/Home4Products";
import Home5Products from "../components/partials/Home/Home5Products";
import SearchModel from "../components/common/SearchModel";

export default function Home(props) {
  const [initialize, setInitialize] = useState(false);
  const [setup, setSetup] = useSetup(props);
  const { banners, products } = props;
  const [type,setType] = useState(1)

  useEffect(() => {
    setInitialize(true);
    let value = props.web_setting.sub_theme == undefined ? 1 : props.web_setting.sub_theme;
    setType(value);
  }, [false]);

  const selectLayout = (type) => {
    if (type == 1) {
      return <Home3Products products={products} />;
    } else if (type == 2) {
      return <Home4Products products={products} />;
    } else if (type == 3) {
      return <Home5Products products={products} />;
    }
  };

  return (
    <div>
      <Slider banners={banners} />
      <VendorDetail />
      <hr />
      <SearchFields />

      {selectLayout(type)}

      <SearchModel products={products} />
      <DetailPopUp />
      <FixedButtonArea />
    </div>
  );
}

export async function getStaticProps() {
  let data = {
    banners: [],
    products: [],
  };

  const response = await networkService.get(urlService.getSetup);

  if (response.IsValid) {
    data = { ...response.Payload };
  }

  // Get external data from the file system, API, DB, etc.
  const homeResponse = await networkService.get(urlService.getHome);

  if (homeResponse.IsValid) {
    const { banners, products } = homeResponse.Payload;

    data.banners = banners;
    data.products = products;
  }
  return {
    props: data,
  };
}
