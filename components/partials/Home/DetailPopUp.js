import React, { useState, useEffect } from "react";
import CloseButton from "./CloseButton";
import { useSelector, useDispatch } from "react-redux";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";
import useCurrency from "../../../services/useCurrency";
import ReactHtmlParser from "react-html-parser";
import SelectAddOn from "./selectAddOn";
import useCart from "../../../services/useCart";
import { removePopup } from "../../../redux/actions/popupAction";

function DetailPopUp() {
  const product = useSelector((state) => state.popup);
  const addons = useSelector((state) => state.addons);
  const store = useSelector((state) => state.store);
  const lang = useLanguage();
  const [count, setCount] = useState(0);
  const [total, setTotal] = useState(0);
  const [increase, setIncrease] = useState(false);
  const currency = useCurrency();
  const [setValue] = useCart({});
  const dispatch = useDispatch();
  const { complete_selection, Error, select_quantity } = useTranslation();

  const increment = () => {
    setCount(count + 1);
    setIncrease(true);
  };

  const decrement = () => {
    setCount(count > 0 ? count - 1 : count);
    setIncrease(false);
  };

  const addToCart = () => {
    let errors = 0;

    if(count == 0){
      toastr.error(select_quantity, Error);
      return;
    }

    

    product[lang].addons.map((addon) => {
      let items = addons.filter((a) => a.grounName == addon.name);
      if (items.length < addon.min) {
        errors++;
      }
    });

    if (errors > 0) {
      toastr.error(complete_selection, Error);
    } else {
      product.addons = addons;

      setTimeout(() => {
        for (let i = 1; i <= count; i++) {
          setValue({ quantity: i, product: product, increment: true });
        }
      }, 500);

      dispatch(removePopup());
      $("#item_detail_popup").modal("hide");
    }
  };

  useEffect(() => {
    let total = count > 0 ? product.retail_price : 0;

    if (addons.length > 0) {
      addons.forEach((addon) => {
        total = parseFloat(total) + parseFloat(addon.price);
      });
    }

    setTotal((total * count).toFixed(store.round_off));

    //setValue({ quantity: count, product: product, increment: increase });
  }, [count, increase, addons]);

  const renderProduct = () => {
    if (Object.keys(product).length == 0 || product[lang] == undefined) {
      return <div className="modal-body"></div>;
    } else {
      return (
        <div className="modal-body">
          <div className="product_detail">
            <div>
              <img
                loading="lazy"
                src={product.image}
                className="w-100 detail_img"
              />
            </div>
            <div className="d-flex p-3 rounded grey m-3 font-bold justify-content-between">
              <span>{product[lang].name}</span>
              <span>
                {currency} {parseFloat(product.retail_price).toFixed(store.round_off)}
              </span>
            </div>
            <div>
              <div className="p-2 pl-4 pr-4">
                {ReactHtmlParser(product[lang].description)}
              </div>
            </div>
            <div className="extra_selection">
              <div id="accordion2" className="accordion  mt-4 mb-4">
                <div className="card  mb-0">
                  {product[lang].addons.map((addon, index) => {
                    return (
                      <SelectAddOn
                        key={addon.name}
                        addon={addon}
                        counter={index}
                      />
                    );
                  })}
                </div>
              </div>
              {/* <SpecialNotes /> */}
            </div>
          </div>
          {/* Fixed Button Area */}
          <div className="btn_fixed_cart">
            <div className="d-flex align-items-center">
              <div className="p-2">
                {currency} : {total}
              </div>
              <div className="p-2">
                <div className="qty_spinner">
                  <form>
                    <button
                      className="value-button minus"
                      id="decrease"
                      onClick={decrement}
                      value="Decrease Value"
                      type="button"
                    >
                      -
                    </button>
                    <input
                      type="number"
                      id="number1"
                      className="number"
                      value={count}
                      disabled="disabled"
                    />
                    <button
                      className="value-button"
                      id="increase"
                      onClick={increment}
                      value="Increase Value"
                      type="button"
                    >
                      +
                    </button>
                  </form>
                </div>
              </div>
              <div className="ml-auto p-2">
                <a href="#" onClick={() => addToCart()}>
                  <span className="iconify" data-icon="bi:cart-plus" />
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };

  return (
    <div
      className="modal fade item_detail_popup"
      id="item_detail_popup"
      tabIndex={-1}
      role="dialog"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog" role="document">
        <div className="modal-content">
          <CloseButton />
          {renderProduct()}
        </div>
      </div>
    </div>
  );
}

export default DetailPopUp;
