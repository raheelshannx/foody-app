import { useSelector } from "react-redux";

function useTranslation() {
  const languages = useSelector((state) => state.languages);
  const translations = useSelector((state) => state.translations);
  const filtered = languages.filter((item) => item.isDefault);
  const currentLanguage = filtered[0];
  return { ...translations[currentLanguage.short_name] }
}

export default useTranslation;
