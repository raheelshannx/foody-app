import React from "react";
import subTitles from "../../common/subTitles";
import FormGroup from "../../common/FormGroup";
import roundedButton from "../../common/roundedButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import urlService from "../../../services/urlService";
import networkService from "../../../services/networkService";
import store from "../../../redux/index";
import axios from "axios";
import qs from "qs";
import Router from "next/router";

function RegisterForm() {
    const {success, Invalid_email_password, customer_register, full_name, email_address, mobile, password, invalid_email, submit, confirm_password, register,name, required } = useTranslation();
const reduxStore = store;
    const onSubmitApi = async (values) => {
        const response = await networkService.post(urlService.postRegister,values);
        console.log(response.Payload.email,values);
        if (response.IsValid) {
            let data = {
              client_id: "11",
              client_secret: "E3WbxaE14ub0GG2GwWSp1NtPpTwUQYcPotcOC1p0",
              username: values.email,
              password: values.password,
              grant_type: "password",
            };

            const uri = "https://services.nextaxe.com/oauth/token?store_id=200";
            // const uri = 'http://localhost:8001/oauth/token?store_id=210';

            let options = {
              method: "POST",
              url: uri,
              data: qs.stringify(data),
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                //"Access-Control-Allow-Origin": "*",
              },
            };

            axios(options)
              .then((response) => {
                localStorage.setItem("token",response.data.access_token)
                Router.push('/');
              })
              .catch(function (error) {
              });


          toastr.success(success);
          setTimeout(() => {
          // const win = window.location.replace('myaccount');
        }, 800);
        }else{
            toastr.error(response.Errors);
        }
        console.log(response);
      }

    return (
        <div
            className="tab-pane fade"
            id="profile"
            role="tabpanel"
            aria-labelledby="profile-tab"
        >
            <Formik
                initialValues={{ name: '', email: '', mobile: '', password: '', confirm_password: '' }}
                validate={values => {
                    const errors = {};
                    if (!values.email) {
                        errors.email = required;
                    } else if (
                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                    ) {
                        errors.email = invalid_email;
                    }
                    if (!values.name) {
                        errors.name = required;
                    }
                    if (!values.mobile) {
                        errors.mobile = required;
                    }
                    if (!values.password) {
                        errors.password = required;
                    }
                    if (values.password != values.confirm_password) {
                        errors.confirm_password = 'Confirm password not matched';
                    }
                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        onSubmitApi(values);
                        setSubmitting(false);
                    }, 400);
                }}
            >

                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                }) => (
                        <form className="regsiter" onSubmit={handleSubmit}>
                            <subTitles title={customer_register} />

                            <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                            <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                            <FormGroup label={email_address} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email_address} />
                            <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                            <FormGroup label={mobile} name="mobile" onChange={handleChange} onBlur={handleBlur} value={values.mobile} hint={mobile} />
                            <p className="text-danger">{errors.mobile && touched.mobile && errors.mobile}</p>

                            <FormGroup label={password} name="password" type="password" onChange={handleChange} onBlur={handleBlur} value={values.password} hint="**********" />
                            <p className="text-danger">{errors.password && touched.password && errors.password}</p>

                            <FormGroup label={confirm_password} name="confirm_password" type="password" onChange={handleChange} onBlur={handleBlur} value={values.confirm_password} hint="**********" />
                            <p className="text-danger">{errors.confirm_password && touched.confirm_password && errors.confirm_password}</p>

                            <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                                {submit}
                            </button>

                        </form>
                    )}
            </Formik>


        </div>
    );
}

export default RegisterForm;
