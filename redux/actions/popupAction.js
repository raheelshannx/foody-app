//set into list action
export const setPopup = (payload) => {
    return {
        type: 'SET_POPUP',
        payload
    }
}

export const removePopup = () => {
    return {
        type: 'REMOVE_POPUP'
    }
}