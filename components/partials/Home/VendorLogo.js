import React from "react";
import { useSelector } from "react-redux";

function VendorLogo() {
    const store = useSelector((state) => state.store);

    return (
        <div className="col-md-4">
            <span className="vendor_logo">
                <img loading="lazy" src={store.logo} className="w-100 logo" />
            </span>
        </div>
    );
}

export default VendorLogo;
