import React from "react";
import useTranslation from "../../../services/useTranslation";

function NavItems() {
    const {login , register} = useTranslation();
    return (
        <ul className="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li className="nav-item">
                <a
                    className="nav-link active"
                    id="home-tab"
                    data-toggle="tab"
                    href="#home"
                    role="tab"
                    aria-controls="home"
                    aria-selected="true"
                >
                    {login}
      </a>
            </li>
            <li className="nav-item">
                <a
                    className="nav-link"
                    id="profile-tab"
                    data-toggle="tab"
                    href="#profile"
                    role="tab"
                    aria-controls="profile"
                    aria-selected="false"
                >
                    {register}
      </a>
            </li>
        </ul>
    );
}

export default NavItems;
