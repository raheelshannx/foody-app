import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/editaccount/whiteWrapper";
import useTranslation from "../services/useTranslation";

export default function EditAccount() {
  const { edit_account } = useTranslation();
  return (
    <div>
      <SubHeader title={edit_account} />
      <WhiteWrapper />
    </div>
  );
}