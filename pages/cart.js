import React from "react";
import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/cart/whiteWrapper";
import useTranslation from "../services/useTranslation";
import FixedButtonArea from "../components/FixedButtonArea";


export default function Cart() {
    const { my_cart } = useTranslation();
    return (
        <div>
            <SubHeader title={my_cart} />
            <WhiteWrapper />
            {/* <FixedButtonArea /> */}
        </div>
    );
}