import { useState, useEffect } from "react";
import ProductGridItem from "../Home/productGridItem";
import useLanguage from "../../../services/useLanguage";
import useTranslation from "../../../services/useTranslation";

//creates a `search` function, which accepts a needle (property name, string),
//a haystack (the object to search within), found (the recursively added to
//list of found properties
const search = (needle, haystack, found = []) => {
  //iterate through each property key in the object
  Object.keys(haystack).forEach((key) => {
    //if the current key is the search term (needle),
    //push its value to the found stack
    if (key === needle) {
      found.push(haystack[key]);
      //return the array of found values to the caller, which is
      //either the caller of the search function, or the recursive
      //"parent" of the current search function
      return found;
    }
    //if the value of the current property key is an object,
    //recursively search it for more matching properties

    //this can be changed to an else if, if properties should not
    //be nested
    if (typeof haystack[key] === "object") {
      search(needle, haystack[key], found);
    }
  });
  //return the list of found values to the caller of the function
  return found;
};

function FilteredProductSection({ category, products }) {
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [init, setInit] = useState(false);
  const [selected, setSelected] = useState(category.id);
  const { all } = useTranslation();

  const lang = useLanguage();

  const handleClick = (category_id) => {
    let filteredProducts = products.filter((p) => {
      return p.category_id === category_id;
    });

    setFilteredProducts(filteredProducts);
  };

  const initSetup = () => {
    if (!init) {
      let ids = search("id", category);

      let filteredProducts = products.filter((prod) => {
        return ids.includes(prod.category_id);
      });

      console.log(category);

      setFilteredProducts(filteredProducts);
      setInit(true);
    }
  };

  const renderTags = () => {
    if (category["children"].length > 0) {
      return (
        <ul className="scrollspyArea">
          <li
            className={category.id == selected ? "active" : ""}
            key={`cat-sub-item-` + category.id}
          >
            <a href="#" onClick={() => handleClick(category.id)}>
              {all}
            </a>
          </li>
          {category["children"].map((sub) => {
            if (sub.id > 0) {
              return (
                <li
                  className={sub.id == selected ? "active" : ""}
                  key={`cat-sub-item-` + sub.id}
                >
                  <a href="#" onClick={() => handleClick(sub.id)}>
                    {sub[lang]}
                  </a>
                </li>
              );
            }
          })}
        </ul>
      );
    }
  };

  useEffect(() => {
    initSetup();
  }, [selected]);

  return (
    <div>
      {/* {renderTags()} */}

      <div className="product_scroller">
        <ul className="inner_pro">
          {filteredProducts.slice(0, 4).map((product) => {
            return <ProductGridItem data={product} key={product.variant_id} />;
          })}
        </ul>
        <hr />
      </div>
    </div>
  );
}

export default FilteredProductSection;
