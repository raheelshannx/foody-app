import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import useTranslation from "../../../services/useTranslation";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useCurrency from "../../../services/useCurrency";

function TotalSummary() {
  const { sub_total, Delivery, total, discount } = useTranslation();
  const cart = useSelector((state) => state.cart);
  const store = useSelector((state) => state.store);
  const [holder, setHolder] = useCheckoutDataHolder();
  const currency = useCurrency();
  const [cartTotal, setCartTotal] = useState(cart.total);
  let [cop_discount_ammount, setDisCopAmount] = useState();

  let is_discount = false;

  if (holder.cop_discount > 0) {
    is_discount = true;
  } else {
    holder.cop_discount = 0;
  }

  useEffect(() => {
    console.log(holder);
    let t = cartTotal;
    if (holder.shippingMethod != null) {
      let t = parseFloat(cart.total) + parseFloat(holder.shippingMethod.rate);
      setCartTotal(t.toFixed(store.round_off));
    }
    if (holder.dis_type != null && holder.dis_type == "amount") {
      holder.cop_discount_ammount = parseFloat(holder.cop_discount);
      t = parseFloat(cart.total) - parseFloat(holder.cop_discount);
      setCartTotal(t.toFixed(store.round_off));
    }
    if (holder.dis_type != null && holder.dis_type == "percentage") {
      t = (parseFloat(cart.total) / 100) * parseFloat(holder.cop_discount);
      holder.cop_discount_ammount = t.toFixed(store.round_off);
      let t1 = parseFloat(cart.total) - t;
      setCartTotal(t1.toFixed(store.round_off));
    }
    setDisCopAmount(holder.cop_discount_ammount);
  }, [holder]);

  const renderShippingTitle = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>{Delivery}</p>
          {/*<p>{"Time"}</p>*/}
          {holder.cop_discount > 0 ? <p>{discount}</p> : ""}
        </>
      );
    }
  };

  const renderShipping = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>
            {currency} {holder.shippingMethod.rate.toFixed(store.round_off)}
          </p>
          {/*<p>{holder.shippingMethod.delivery_time}</p>*/}
          {holder.cop_discount > 0 ? (
            <p>
              {" "}
              {currency} - {holder.cop_discount_ammount}
            </p>
          ) : (
            ""
          )}
        </>
      );
    }
  };

  const handleClick = () => {};

  return (
    <div>
      <div className="d-flex p-4 grey mt-2 mb-5 justify-content-between cart_total">
        <span>
          <p>{sub_total}</p>
          {renderShippingTitle()}
          <p>
            <b>{total}</b>
          </p>
        </span>
        <span className="text-right">
          <p>
            {currency} {cart.total}
          </p>
          {renderShipping()}
          <p>
            <b>
              {currency} {cartTotal}
            </b>
          </p>
        </span>
      </div>
    </div>
  );
}

export default TotalSummary;
