import React, { Component, useRef, useState, useEffect } from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
function LocationMap({ latlong }) {

  const mapRef = useRef(null);
  const [location, setLocation] = useState();
  const [holder, setHolder] = useCheckoutDataHolder();
  useEffect(() => {
    setLocation(JSON.parse(latlong));
  },[latlong]);
console.log(location);
  function handleCenterChanged() {
    if (!mapRef.current) return;
    const newPos = mapRef.current.getCenter().toJSON();
    setLocation(newPos);
    console.log(newPos);
  }

  const MapWithAMarker = withScriptjs(withGoogleMap(props =>
    <GoogleMap
      ref={mapRef}
      defaultZoom={15}
      defaultCenter={location}
      onCenterChanged={handleCenterChanged}
    >
      <Marker
        position={location}
      />
    </GoogleMap>
  ));
  return (
    <div className="map mb-5" style={{ width: '94%', height: '200px' }}>
      <MapWithAMarker
        id="latlng"
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRCd_mF3VtrWp8rdRtnOjZkdE9P_-kxRc&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `200px` }} />}
        mapElement={<div style={{ height: `200px` }} />}
      />
    </div>
  );
}
export default LocationMap;


