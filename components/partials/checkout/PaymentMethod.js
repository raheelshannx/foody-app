import React, { useState ,useEffect} from "react";
import RadioButton from "./radioButton";
import { useSelector } from "react-redux";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import { Formik, Field, Form } from "formik";


function PaymentMethod() {
  const paymentMethods = useSelector((state) => state.paymentMethods);
  const [holder, setHolder] = useCheckoutDataHolder();
  const [selected, setSelected] = useState('payza');

  const setPaymentMethod = (event) => {
    console.log(event.target.checked);
    // holder.paymentMethod = paymentMethod;
    // setHolder(holder);
    //setSelected(event.target.value);
  };

  useEffect(()=>{

  },[selected])

  return (
    <ul className="payment_method">
      {paymentMethods.map((method, index) => {
        let id = `payment-${method.slug}`;
        let idHash = `#payment-${method.slug}`;

        return (
          <li key={method.slug}>
            <div className="custom-control custom-radio">
              <input
                type="radio"
                id={id}
                name="paymentMethod"
                className="custom-control-input"
                onChange={(event) => setPaymentMethod(event)}
                value={method.slug}
                checked={method.slug == selected}
                defaultChecked
              />
              <label className="custom-control-label" htmlFor={idHash}>
                {method.name}
              </label>
            </div>
          </li>
        );
      })}
    </ul>
  );
}
export default PaymentMethod;
