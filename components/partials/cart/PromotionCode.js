import React, { useState, useEffect } from "react";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";


function PromotionCode() {
    const store = useSelector((state) => state.store);
    const { promotioncode, enterpromotioncode } = useTranslation();
    const plugins = useSelector((state) => state.plugins);
    
    const [holder, setHolder] = useCheckoutDataHolder();
    let [dis_type, setDisType] = useState()
    let [cop_discount, setDisCop] = useState()
    let [discount_voucher_code, setDisVou] = useState()
    let dis_plug = {};
    holder.dis_type = 'percentage';
    // holder.cop_discount = (5.234234).toFixed(store.round_off);
    const handleClick = () => {
        let plugi = dis_plug;
        let voucher = $('#discount_voucher').val();
        console.log(plugi,voucher);
        holder.discount_voucher_code = voucher;
        if(voucher.length > 0) {

            let url = plugi.api_url;
            let token = plugi.api_token;
            var settings = {
                "url": url + "vouchers/verify/" + voucher,                                  
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": "Bearer " + token,
                    "Accept": "application/json"
                },
            };

            let currency = 'KWD';

            $.ajax(settings).done(function (response) {
                console.log(response);
                if(response.data == null) {
                    $('.voucher-error').html('Invalid coupon');
                    $('.voucher-error').removeClass('d-none');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-danger');
                    holder.dis_type = '';
                    holder.cop_discount = '';
                }
                if(response.status == 'valid') {
                    $('.voucher-error').html('Coupon Applied');
                    $('.voucher-error').removeClass('d-none');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-warning');
                    $('.voucher-error').addClass('text-success');
                    $('#discount_voucher_code').val(voucher);
                    $('.discount_price').removeClass('d-none');
                    $('.discount_price_val').removeClass('d-none');
                    if(response.data.discount_type == 1) {
                        holder.dis_type = 'amount';

                        holder.cop_discount = response.data.discount_amount.toFixed(store.round_off);

                    } else if(response.data.discount_type == 2) {
                        holder.dis_type = 'percentage';
                        holder.cop_discount = response.data.discount_amount.toFixed(store.round_off)
                    }

                } else if(response.status == 'expired') {
                    $('.voucher-error').html('Coupon expired');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-warning');
                    $('#discount_voucher_code').val();
                    holder.dis_type = '';
                    holder.cop_discount = '';
                } else if(response.status == 'limit_reached') {
                    holder.dis_type = '';
                    holder.cop_discount = '';
                    $('.voucher-error').html('Coupon usage limit reached');
                    $('.voucher-error').removeClass('text-danger');
                    $('.voucher-error').removeClass('text-success');
                    $('.voucher-error').addClass('text-warning');
                    $('#discount_voucher_code').val();
                }
            }).fail(function (jqXHR, textStatus) {
                console.log(textStatus);
            });
        }

    }

    useEffect(() => {
        setDisType(holder.dis_type );
        setDisCop(holder.cop_discount );
        setDisVou(holder.discount_voucher_code );
      },[holder]);

    const renderDiscountCode = () => {
        let is_true = false;
        dis_plug = {};
        plugins.map((plugin) => {
            if(plugin.plugin != null && plugin.plugin.slug == 'discount'){
                is_true = true;
                dis_plug = plugin;
            }
        })
        if(is_true){
            return (
                <div className="">
                  <h2 className="text-uppercase pt-4 pb-3">Promotion Code</h2>
                  <div className="form-row">
                    <div className="col-auto">
                      <input
                        id="discount_voucher"
                        type="text"
                        placeholder="Enter Promotion Code"
                        className="form-control col-xs-8"
                        name=""
                        required
                      />
                    </div>
                    <div className="col-auto ">
                      <button
                        className="btn btn-primary pull-right col-xs-4 m-1"
                        type="button"
                        onClick={() => handleClick()}
                        style={{
                          background: "#2D2F41",
                          backgroundColor: "#2D2F41",
                          borderColor: "#2D2F41",
                        }}
                      >
                        Apply
                      </button>
                    </div>
                    <span className="d-none error text-danger voucher-error"></span>
                  </div>
                </div>
            );
        }
    };

    return (
        <div>
            {renderDiscountCode()}
        </div>
    );
}
export default PromotionCode;
